extern crate sdl2;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::keyboard::Keycode;
use sdl2::EventPump;
use sdl2::rect::Rect;
use std::hash::{Hash, Hasher};
use std::collections::HashSet;
use sdl2::rect::Point;

use fontdue::layout::{CoordinateSystem, Layout, TextStyle};
use fontdue::Font;
use fontdue_sdl2::FontTexture;

fn in2mm(inch: f32) -> f32 {
    return inch * 2.54*10.0;
}
fn ft_in2mm(ft: i64, inch: i64) -> f32 {
    return in2mm(12.0*ft as f32) + in2mm(inch as f32);
}

const BUS_WIDTH: f32 = 2387.6;
const REAR_STICKOUT: f32 = 300.0;
const BUS_LENGTH: f32 = 12192.0 + REAR_STICKOUT; //11938.0;
const _EPSILON: f32 = 5.0;

const SCALE: f32 = 0.1;
const WIN_W: u32 = (SCALE*BUS_WIDTH) as u32;
const WIN_H: u32 = (SCALE*BUS_LENGTH) as u32;

fn draw_panel(p: &Panel, canvas: &mut Canvas<Window>) {
    canvas.set_draw_color(p.color);
    let rect = Rect::new((SCALE*p.col).round() as i32, 
                         (SCALE*p.row).round() as i32, 
                         (SCALE*p.col_size).round() as u32, 
                         (SCALE*p.row_size).round() as u32);

    let _ = canvas.fill_rect(rect);
    canvas.set_draw_color(Color::RGB(0,0,0));
    let _ = canvas.draw_rect(rect);
    //canvas.present();
    //std::thread::sleep(std::time::Duration::from_millis(1000));
}

fn draw_x(row: f32, col: f32, canvas: &mut Canvas<Window>) {
    canvas.set_draw_color(Color::RGB(200,0,0));
    let mut size = SCALE*700.0;
    let l1 = Point::new((SCALE*(col-size)).round() as i32, (SCALE*(row-size)).round() as i32);
    let l2 = Point::new((SCALE*(col+size)).round() as i32, (SCALE*(row+size)).round() as i32);
    let l3 = Point::new((SCALE*(col+size)).round() as i32, (SCALE*(row-size)).round() as i32);
    let l4 = Point::new((SCALE*(col-size)).round() as i32, (SCALE*(row+size)).round() as i32);

    canvas.draw_line(l1,l2);
    canvas.draw_line(l3,l4);

    //canvas.present();
    //std::thread::sleep(std::time::Duration::from_millis(500));
}
fn update_stat_text(text: &str, font_texture: &mut FontTexture, layout: &mut fontdue::layout::Layout<Color>, fonts: &[Font], statcanvas: &mut Canvas<Window>) {
    layout.clear();
    layout.append(fonts, &TextStyle::with_user_data(
        text,           // The text to lay out
        32.0,                      // The font size
        0,                         // The font index (Roboto Regular)
        Color::RGB(0xFF, 0xFF, 0)  // The color of the text
    ));
    statcanvas.clear();
    let _ = font_texture.draw_text(statcanvas, fonts, layout.glyphs());
    statcanvas.present();
}
fn main() {

    // window stuff 
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem.window("robengine", WIN_W, WIN_H)
         .position_centered()
         .build()
         .unwrap();
    let statwindow = video_subsystem.window("stats", 300,200).position(1600,600).build().unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    
    let mut statcanvas = statwindow.into_canvas().build().unwrap();
    statcanvas.set_draw_color(Color::RGB(0,0,0));
    statcanvas.clear();
    statcanvas.present();
    
    let font = include_bytes!("/usr/share/fonts/truetype/ubuntu/UbuntuMono-B.ttf") as &[u8];
    let roboto_regular = Font::from_bytes(font, Default::default()).unwrap();
    let fonts = &[roboto_regular];
    let mut flayout = Layout::new(CoordinateSystem::PositiveYDown);
    flayout.append(fonts, &TextStyle::with_user_data(
        "Hello, World!",           // The text to lay out
        32.0,                      // The font size
        0,                         // The font index (Roboto Regular)
        Color::RGB(0xFF, 0xFF, 0)  // The color of the text
    )); 

    let texture_creator = statcanvas.texture_creator();
    let mut font_texture = FontTexture::new(&texture_creator).unwrap();
    let _ = font_texture.draw_text(&mut statcanvas, fonts, flayout.glyphs());
    statcanvas.present();
    let mut highest_w_seen: f32 = 0.0;
    update_stat_text(&highest_w_seen.to_string(), &mut font_texture, &mut flayout, fonts, &mut statcanvas);

    // bus stuff 
    let l1: f32 = ft_in2mm(19,6);
    let l2: f32 = ft_in2mm(21,11);
    let l3: f32 = ft_in2mm(39,2);
    
    //let bus_width: f32  = ft_in2mm(7,10);
    let bus_width = BUS_WIDTH;

    let _w_hatch = ft_in2mm(2,9);

    let _l_front = l1;
    let _l_rear  = l3-l2;

    println!("bus width: {:?}", bus_width);
   
    let mut hatch = Panel::with_color(ft_in2mm(2, 5), ft_in2mm(2, 4), 0.0, "Hatch".to_string(), Color::RGB(0,0,0));
    hatch.row = ft_in2mm(19, 6);
    hatch.col = ft_in2mm(2, 5);
    let _pa = Panel::with_color(1100.0, 1800.0, 430.0, "Robs A".to_string(), Color::RGB(200,0,200));
    let _pb = Panel::with_color(1200.0, 2200.0, 400.0, "Robs B".to_string(), Color::RGB(50,50,200));
    let _pc = Panel::with_color(800.0, 600.0, 240.0, "Robs C".to_string()  , Color::RGB(0,200,50));
    
    //2.1 X 41.5", 
    let missionsolar = Panel::with_color(in2mm(82.1), in2mm(41.5), 430.0, "Mission Solar 430".to_string(),  Color::RGB(50,50,255));
    let rec          = Panel::with_color(in2mm(71.7), in2mm(40.0), 410.0, "Rec410AA Pure".to_string(),      Color::RGB(200,0,200));
    let cansolar     = Panel::with_color(in2mm(76.4), in2mm(41.5), 395.0, "cs3n-395ms".to_string(),         Color::RGB(50,50,200));
    let silfab       = Panel::with_color(in2mm(82.6), in2mm(44.6), 500.0, "sil-500-hm".to_string(),         Color::RGB(0,200,50));
    let jasolar      = Panel::with_color(in2mm(67.8), in2mm(44.65),395.0, "jam54-s32-395mr".to_string(),    Color::RGB(0,255,0));
    let cansolar550  = Panel::with_color(2266.0,      1134.0,      550.0, "CS6W-550MB-AG".to_string(),      Color::RGB(255,0,0));

    let panels: Vec<Panel> = Vec::from([cansolar550]);
    let mut panels_with_rots_gen = panels.clone();
    for p in panels {
        panels_with_rots_gen.push(p.rot());
    }
    let panels_with_rots = panels_with_rots_gen.clone();
    for p in &panels_with_rots {
        println!("{}, \t{}, \t{}", p.name, p.col_size, p.row_size);
    }
    println!("");

    let mut visited_layouts: HashSet<Vec<Panel>> = HashSet::new();
    let mut finished_layouts: Vec<Vec<Panel>> = Vec::new(); 
    for p in &panels_with_rots {

        // if panel fits across width start a layout with it
        // else throw away, its rotation comes too.
        if p.col_size < bus_width {
            println!("creating layout with {}", p.name);       
            let layout: Vec<Panel> = Vec::from([p.clone(), hatch.clone()]);
            let mut prefix = "".to_string();

            let first_time_seen = visited_layouts.insert(layout.clone());
            println!("first_time_seen: {:?}", first_time_seen);
            if first_time_seen {
                create_layouts(&panels_with_rots, layout, &mut finished_layouts,&mut prefix, &mut event_pump, &mut canvas, &mut visited_layouts, &mut highest_w_seen, &mut font_texture, &mut flayout, fonts, &mut statcanvas );
            }
        } else {
            println!("{} is too wide to fit acros bus", p.name);
        }

    }
    
    let mut tot_w = 0.0;
    finished_layouts.sort_by( |l2, l1| get_total_w(&l1).partial_cmp(&get_total_w(&l2)).unwrap() );
    
    for l in finished_layouts {
        canvas.set_draw_color(Color::RGB(255,255,255));
        canvas.clear();
        println!("Layout...");
        for p in &l {
            println!("{:?}", p.name);
            tot_w += p.wattage;
            draw_panel(&p, &mut canvas);

        }
        canvas.present();
        println!("tot_w: {}", tot_w);
        println!("Layut len: {}",l.len());
        tot_w = 0.0;
        std::thread::sleep(std::time::Duration::from_millis(100));
    }
}

fn get_total_w(layout: &Vec<Panel>) -> f32 {
    let mut totw = 0.0;
    for p in layout {
        totw+=p.wattage;
    }
    return totw;
}



fn create_layouts(panels_with_rots: &Vec<Panel>, layout: Vec<Panel>, finished_layouts: &mut Vec<Vec<Panel>>, prefix: &mut String, event_pump: &mut EventPump, canvas: &mut Canvas<Window>, visited_layouts: &mut HashSet<Vec<Panel>>, highest_w_seen: &mut f32, font_texture: &mut FontTexture, flayout: &mut fontdue::layout::Layout<Color>, fonts: &[Font], statcanvas: &mut Canvas<Window>) {
    //let cur_panel = layout.last().unwrap();
    for event in event_pump.poll_iter() {
        match event {
                 Event::Quit {..} |
                 Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                     panic!("whoa...");
                     //break;
                 },
                 _ => {}
        }
    }
    if layout.len() > 20 {
        println!("\n");
        println!("{:?}", layout);
        canvas.set_draw_color(Color::RGB(255,255,255));
        canvas.clear();
        for p in &layout {
            draw_panel(p,canvas); 
            let c = get_panel_corners(p);
            draw_x(c[2].0, c[2].1, canvas);
            draw_x(c[1].0, c[1].1, canvas);
            canvas.present();
            std::thread::sleep(std::time::Duration::from_millis(1000));
        }
        canvas.present();
        std::thread::sleep(std::time::Duration::from_millis(5000));
        panic!("too many panels...");
    }

    // sanity check
    //for p in &layout {
    //    for p2 in &layout {
    //        if p == p2 { continue; }
    //        if panels_overlapping(p,p2) {
    //            println!("Panels overlapping! Issue!");
    //            println!("\n{:?}\nand\n{:?}",p,p2);
    //            std::thread::sleep(std::time::Duration::from_millis(5000));
    //            panic!("hm.");
    //        }
    //    }
    //}

    canvas.set_draw_color(Color::RGB(255,255,255));
    canvas.clear();
    //canvas.present();
    for p in &layout {
        draw_panel(p,canvas);
    }
    let totw = get_total_w(&layout);
    if totw > *highest_w_seen { *highest_w_seen = totw; }
    update_stat_text(&highest_w_seen.to_string(), font_texture, flayout, fonts, statcanvas);


    //canvas.present();
    //std::thread::sleep(std::time::Duration::from_millis(1000));
    let mut layout_changed = false;
  
    //println!("\n{}New layout:",prefix);
    //for p in &layout { p.print(); }

    //let mut rng = rand::thread_rng();
    //let y: f32 = rng.gen();

    // try, and check for overlaps, all SW and NE corners.
    // If trying a SW corner look to the left until hit something (later) 
    // vecs to fill with possible points 
    let mut sw_vec: Vec<(f32, f32)> = Vec::new();
    let mut ne_vec: Vec<(f32, f32)> = Vec::new();
    let mut nw_vec: Vec<(f32, f32)> = Vec::new(); // only used for ruling out free locations early.
    // Obtain all NE and SW corners in current layout.
    for p in &layout {
        let c = get_panel_corners(p);
        draw_x(c[2].0, c[2].1 ,canvas);
        draw_x(c[1].0, c[1].1,  canvas);
        sw_vec.push( c[2] );
        ne_vec.push( c[1] );
        nw_vec.push( c[0] );
    }
    canvas.present();
    // if a NE corner also exists as a NW then the spot to its right is def filled so remove it.
    let mut ne_temp_vec: Vec<(f32, f32)> = Vec::new();
    for ne in &ne_vec {
        let mut overlap = false;
        for nw in &nw_vec {
            if ne == nw {
                overlap=true;
                break;
            }
        }
        if !overlap {
            ne_temp_vec.push(*ne);
        }
    }
    ne_vec = ne_temp_vec;
    // if a SW corner also exists as a nw then that pocket is also filled, remove it.
    let mut sw_temp_vec: Vec<(f32, f32)> = Vec::new();
    for sw in &sw_vec {
        let mut overlap = false;
        for nw in &nw_vec {
            if sw == nw {
                overlap = true;
                break;
            }
        }
        if !overlap {
            sw_temp_vec.push(*sw);
        }
    }
    sw_vec = sw_temp_vec;

    // Start iterating over panels to try and place 
    for p in panels_with_rots {
        let mut free_locations: Vec<(f32, f32)> = Vec::new();

        // try all NE locations
        for c in &ne_vec {
            let mut free = true;
            let mut testp = p.clone();
            testp.row = c.0; // + EPSILON +rng.gen::<f32>() ;
            testp.col = c.1; // + EPSILON +rng.gen::<f32>() ;
            for pl in &layout {
                if panels_overlapping(&testp, pl) {
                    free = false;
                    break;
                }
            }
            if free {
                free_locations.push(*c);
            }
        }
        // try all SW locations
        for c in &sw_vec {
            let mut free = true;
            let mut testp = p.clone();
            testp.row = c.0; // + EPSILON +rng.gen::<f32>();
            testp.col = c.1; // + EPSILON +rng.gen::<f32>();
            //for pl in &layout {
            //    if panels_overlapping(&testp, pl) {
            //        free = false;
            //        break;
            //    }
            //}
            if free {   // No collisions were detected, probably good place, still need to look left.
                free_locations.push(*c);
            }

            // check left of all SW corners, might be free. 
            let mut possible_moves: Vec<Panel> = Vec::new();
            for pl in &layout {
                let cl = get_panel_corners(pl);
                let pl_ne = cl[NE];
                let pl_se = cl[SE];
                let _pl_nw = cl[NW];
                let _pl_sw = cl[SW];
        
                //if pl_ne.1 >= testp.col { // this panel is to the right, throw away point
                //    continue;
                //}
                //if pl_se.0 <= testp.row { // this panel is fully above, throw away.
                //    continue;
                //}
                if pl_ne.0 > testp.row+testp.row_size { // this is fully below, throw away 
                    continue;
                }
                // this panel might be one of them, but we want to check the closest one.
                possible_moves.push(pl.clone());
                //println!("{}Adding pl to possible_moves",prefix);
            }
            // we now need to find the closest move, if any 
            if !possible_moves.is_empty() {
                //println!("{}Attempting move of points....",prefix);
                let mut closest_p: Panel = possible_moves.pop().unwrap();
                //println!("{}{:?}",prefix,closest_p);
                for p in possible_moves {
                    // 
                    if testp.col-(p.col+p.col_size) < testp.col-(closest_p.col+closest_p.col_size) {
                        closest_p = p;
                        //println!("Moved! {:?}",closest_p);
                    }
                }
                free_locations.push((testp.row, closest_p.col));
            }

        }
        free_locations.sort_by(|a, b| a.partial_cmp(b).unwrap());
        free_locations.dedup();

        //println!("free_locations: {:?}", free_locations);

        // now we iterate through the free locations, if panel doesn't stick out then we place it
        // there.
        if free_locations.len() > 0 {
            for loc in free_locations {
                let try_width = loc.1 + p.col_size;
                let try_length = loc.0 + p.row_size;
                //println!("{}looking at panel {} at ({}, {}), which has try_width {} and try_lengt {}", 
                //        prefix, p.name, loc.0, loc.1, try_width, try_length);
                //print!("{}",prefix);
                //p.print();
                if try_width < BUS_WIDTH  && try_length < BUS_LENGTH {
                    //println!("{}Adding {} to next col on bus", prefix, p.name);
                    // make new layout for this combo
                    let mut new_layout = layout.clone();
                    // copy panel and adjust its position 
                    let mut new_panel = p.clone();
                    new_panel.col = loc.1; // + EPSILON ;
                    new_panel.row = loc.0; // + EPSILON;
                    //draw_panel(&new_panel, canvas);
                    new_layout.push(new_panel.clone());
                    prefix.push_str(".");
                    //std::thread::sleep(std::time::Duration::from_millis(500));
                    
                    let first_time_seen = visited_layouts.insert(new_layout.clone());
                    //println!("{}first_time_seen for new_layout about to be added: {}",prefix,first_time_seen);
                    if first_time_seen {

                        //double check overlaps...
                        let mut safe_add = true;
                        for testp in &layout {
                            let overlap = panels_overlapping(&new_panel, testp);
                            //print!("{} ",overlap);
                            if overlap {
                                //println!("Shouldn't happen!");
                                safe_add = false;
                            }
                        }
                        if safe_add{
                            //std::thread::sleep(std::time::Duration::from_millis(5000));
                            create_layouts(panels_with_rots, new_layout, finished_layouts, prefix, event_pump, canvas, visited_layouts, highest_w_seen, font_texture, flayout, fonts, statcanvas);
                        layout_changed = true;
                        } 
                    } else {
                        println!("{}This layout has been seen before, skipping.",prefix);
                        //layout_changed = false;
                    }
                } else {
                    //println!("{}{} will stick out, skipping.", prefix, p.name);
                }
            }
        } else {
            layout_changed = false;
        }
    }
        
        // if we didn't add any panels at all then we should dump the layout stats now.
    if !layout_changed {
        println!("\nFinished layout!");
        let mut tot_w: f32 = 0.0;
        for p in &layout {
            tot_w = tot_w + p.wattage;
            println!("{}::{}:\t({}, {})",prefix.len(),p.name,p.row,p.col);
            //draw_panel(p,canvas);
            //std::thread::sleep(std::time::Duration::from_millis(1000));
        }
        println!("tot_w: {}", tot_w);
        finished_layouts.push(layout);
    }
}

fn get_panel_corners(p: &Panel) -> Vec<(f32, f32)> {
    let c1 = (p.row,            p.col               ); // NW
    let c2 = (p.row,            p.col+p.col_size    ); // NE 
    let c3 = (p.row+p.row_size, p.col               ); // SW 
    let c4 = (p.row+p.row_size, p.col+p.col_size    ); // SE 
    return Vec::from([c1,c2,c3,c4]);
}
const NW: usize = 0;
const NE: usize = 1;
const SW: usize = 2;
const SE: usize = 3;
const _ROW: usize = 0;
const _COL: usize = 1;


fn panels_overlapping(p1: &Panel, p2: &Panel) -> bool {
    let c1 = get_panel_corners(p1);
    let c2 = get_panel_corners(p2);

    if c1 == c2 {
        //panic!("YAY!");
        println!("Perfect panel corner match!");
        return true;

    }

    if  c1[NW].1 < c2[NE].1 && c1[NE].1 > c2[NW].1 &&
        c1[NW].0 < c2[SW].0 && c1[SW].0 > c2[NW].0  {
        return true;
    }

    return false;
    
    // if (RectA.Left < RectB.Right && RectA.Right > RectB.Left &&
    // RectA.Top > RectB.Bottom && RectA.Bottom < RectB.Top )
}
// 40in smallest dim panels, 1016mm
// 11201.4mm long bus
// 12 panels at 1016mm is 12192mm which is just > 11201.4.
 

#[derive(std::clone::Clone, Debug)]
struct Panel{
    name: String,
    row: f32,
    col: f32,
    col_size: f32,
    row_size: f32,
    wattage: f32,
    watt_density: f32,
    color: Color,
}
impl PartialEq for Panel {
    fn eq(&self, other: &Self) -> bool {
        let eps: f32 = 100.0;
        if (self.col_size   - other.col_size    ).abs() > eps { return false; }
        if (self.row_size   - other.row_size    ).abs() > eps { return false; }
        if (self.col        - other.col         ).abs() > eps { return false; }
        if (self.row        - other.row         ).abs() > eps { return false; }
        if (self.wattage    - other.wattage     ).abs() > eps { return false; }
        // watt density not needed to check, derived value.
        // we don't care for name, if size, watt and location are the same its enough.
        // we also don't care for color since they are already equivalent performing.
        return true;
    }
}
impl Eq for Panel{}
impl Hash for Panel {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let precision: f32 = 0.1;
        ((self.col_size *precision).round() as i64).hash(state);
        ((self.row_size *precision).round() as i64).hash(state);
        ((self.col      *precision).round() as i64).hash(state);
        ((self.row      *precision).round() as i64).hash(state);
        ((self.wattage  *precision).round() as i64).hash(state);
    }
}
impl Panel {
    pub fn new(a: f32, b: f32, watt: f32) -> Panel {
        return Panel{
            col_size: b,
            row_size: a,
            col: 0.0,
            row: 0.0,
            wattage: watt,
            watt_density: watt/(0.001*0.001*a*b),
            name: "".to_string(),
            color: Color::RGB(0, 0, 0),
        };
    }
    pub fn with_name(a: f32, b: f32, watt: f32, name: String) -> Panel {
        let mut p = Panel::new(a,b,watt);
        p.name = name;
        return p;
    }
    pub fn with_color(a: f32, b: f32, watt: f32, name: String, color: Color) -> Panel {
        let mut p = Panel::with_name(a,b,watt,name);
        p.color = color;
        return p;
    }
    
    pub fn rot(&self) -> Panel {
        let mut p = self.clone();
        p.col_size = self.row_size;
        p.row_size = self.col_size;
        p.name = p.name+"_rot";
        return p;
    }
    pub fn print(&self) {
        println!("{:20}:\t({:07}, {:07})\trow_size:{:07}\tcol_size:{:07}\tw:{}",
                self.name, self.row, self.col, self.row_size, self.col_size, self.wattage);
    }
}

