# Solar Bus 
This is an optimizer to help me find a good layout and type of solar panels to cover the roof of our motorhome.
It is written in rust, utelizing sdl2.

## General logic
The program recursively adds panels from the list of available panels (and their 90 deg rotations) and checks if this layout (with some precision) has been seen before or not.
Once it has filled the roof with panels and no new panels can be added it saves the layout and at the end of the program the layouts will be presented in order of total watts in the layout.

## Other notes 
While the program does allow you to mix and match panels be sure you are ready for the electrical challanges you will face if you do.

The code uses 0 watt panels to create obstacles on the roof where panels cannot be mounted, such as the emergency roof hatch in the middle of the motorhome.
There are variables for maximum allowed panel stickout in the rear.

The checks as to if panels are overlapping or where a new panel can be placed is not very straightforward, but quite efficient.

